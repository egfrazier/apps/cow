# Overview

This project is an unoffical GraphQL API for the Correlates of War project, a collection of data on international wars and other conflicts dating back to 1816. The API is a Flask application designed to connect to a Relational Database Management System. It also uses the `hvac` libary to connect to an external secrets management service (specifically for database credentials). Supported `hvac` services include Hashicorp Vault and AWS Secrets Manager.

# Development

To run locally, you should have Docker and Docker Compose installed. Along with the following environmental variables set:
* `COW_API_DB_PASSWORD`
* `COW_API_NETWORK` (I set this to a custom Docker network where I keep generalized containers that other projects use (e.g. a Hashicorp Vault container). If you don't need to connect your COW API containers to other containers in this manner, you can just set this environmental variable to `bridge`.)

To start the command, run the following `make` command from project root:
```
make dc-up
```

Then run the following to initialize and populate the database:
```
$ make init-db
$ make load-db
```

You can experiment with API queries using GraphiQL by opening `http://localhost:5000` in your web browser.