from flask import Flask
import pytest

from cow import create_app
from cow.db import get_db

class BaseTestCase(TestCase):
    def create_test_app(self):
        config_map = {
            'DATABASE': {
                'USER': 'postgres',
                'SQLA_TRACKING': False,
            },
            'DATA_SOURCES': {
                'COUNTRY_CODES': 'http://cowapi.org/path/to/data'
            }
        }

        return  create_app(config_map)