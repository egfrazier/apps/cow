from flask import Blueprint, render_template, abort
from flask_cors import CORS, cross_origin
from flask_graphql import GraphQLView
from graphene import Schema

from cow.gql.query import StateQuery
from cow.gql.query import SystemMemberQuery

class Query(StateQuery, SystemMemberQuery):
    pass

gql_endpoint = Blueprint('gql_endpoint', __name__)

CORS(gql_endpoint)

gql_endpoint.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=Schema(query=Query),
        graphiql=True
    )
)