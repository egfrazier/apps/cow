from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from cow.models import State as StateModel

class State(SQLAlchemyObjectType):
    class Meta:
        model = StateModel
        interfaces = (relay.Node, )
