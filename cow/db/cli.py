import os

import click
from flask import current_app, g
from flask.cli import with_appcontext

from cow.core.secret import get_secret

from .db import get_db


def init_db():

    with current_app.app_context():
        from cow.models import db, State, SystemMember

        db.create_all()


def load_db():
    db = get_db()

    from cow.jobs import load_db

    load_db.run()


@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Initialized the database.')


@click.command('load-db')
@with_appcontext
def load_db_command():
    load_db()
    click.echo('COW data loaded into database.')

@click.command('psql')
@with_appcontext
def psql():
    # `database` is defined by the links feature
    # in docker-compose
    os.environ['PGPASSWORD'] = get_secret('cow-api-db')['data']['data']['secret']
    os.system('psql -h database -U postgres -p 5432 -d cow')
    os.environ['PGPASSWORD'] = ''