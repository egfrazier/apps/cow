from flask import current_app, g
from flask_sqlalchemy import SQLAlchemy

from cow.core.secret import get_secret

def get_db():
    if 'db' not in g:
        if not current_app.config['TESTING']:
            db_cfg = current_app.config['DATABASE']
            db_host = db_cfg.get('HOST', 'database')
            db_user = db_cfg['USER']
            db_pass = get_secret('cow-api-db')['data']['data']['secret']
            sqla_tracking = db_cfg['SQLA_TRACKING']
            current_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = sqla_tracking

            current_app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:5432/cow'
        else:
            current_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

        g.db = SQLAlchemy(current_app)

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close_all_sessions()
