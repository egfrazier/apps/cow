import os

from flask import Flask, make_response
from flask_sqlalchemy import SQLAlchemy
import toml

def create_app(test_config=None):
    app = Flask(__name__)


    if test_config is None:
        app.config.from_file('../etc/config.toml', load=toml.load)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # TODO: Why do we have to manually push an app context
    # When we already have the application context for
    # registering blueprints?
    with app.app_context():
        from cow.blueprints.graphql import gql_endpoint
        app.register_blueprint(gql_endpoint)


    # Routes
    @app.route('/')
    def hello():
        return '''
Hello! Welcome to the Correlates of War API<br />
Please use the /graphql endpoint to query data.<br />
Source code is located <a href="https://gitlab.com/egfrazier/apps/cow">here</a>
'''

    from .db import init_app
    init_app(app)
    
    return app

