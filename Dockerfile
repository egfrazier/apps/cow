# syntax=docker/dockerfile:1
FROM python:3.9-alpine
RUN apk update
RUN apk add --no-cache g++ gcc musl-dev linux-headers postgresql-dev postgresql-client

RUN addgroup app
RUN adduser \
    --disabled-password \
    --home /code \
    --gecos "" \
    --ingroup app \
    app
USER app

WORKDIR /code
ENV VIRTUAL_ENV=/code/.venv
RUN /usr/local/bin/python -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --upgrade pip wheel
COPY requirements/requirements.txt requirements/requirements.txt
COPY requirements/dev-requirements.txt requirements/dev-requirements.txt
RUN pip install -r requirements/requirements.txt -r requirements/dev-requirements.txt
EXPOSE 5000
COPY . .
ENV FLASK_APP=cow
ENV FLASK_ENV="$COW_API_ENV"
CMD ["./deploy/deploy.sh"]