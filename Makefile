dc-up:
	docker-compose up --build

dc-down:
	docker-compose down --rmi all

init-db:
	docker exec -it cow-api-web-1 flask init-db

load-db:
	docker exec -it cow-api-web-1 flask load-db

coverage:
	docker exec -it cow-api-web-1 coverage run -m pytest